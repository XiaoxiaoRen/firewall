package firewall;

import java.util.ArrayList;
import java.util.List;

public class Rule {

    String direction;
    String protocol;
    List<Long> portRange;
    List<Long> ipRange;

    public Rule(String dir, String pro, String port, String ip) {

        direction = dir;
        protocol = pro;
        portRange = new ArrayList<>();
        ipRange = new ArrayList<>();

        if (port.contains("-")) {
            String[] range = port.split("-");

            if (range.length != 2) System.out.println("Invalid port range");

            portRange.add(Long.valueOf(range[0]));
            portRange.add(Long.valueOf(range[1]));
        } else {
            long portInt = Long.valueOf(port);
            portRange.add(portInt);
            portRange.add(portInt);
        }

        if (ip.contains("-")) {
            String[] range = ip.split("-");

            if (range.length != 2) System.out.println("Invalid ip range");

            ipRange.add(getIp(range[0]));
            ipRange.add(getIp(range[1]));
        } else {
            long ipValue = getIp(ip);
            ipRange.add(ipValue);
            ipRange.add(ipValue);
        }
    }

    public static long getIp(String ip) {
        long ipValue = 0;

        String[] addrStr = ip.split("\\.");

        if (addrStr.length != 4) System.out.println("Invalid IP address");

        for (int i = 0; i < 4; i++) {
            ipValue *= 256;
            ipValue += Long.valueOf(addrStr[i]);
        }

        return ipValue;
    }

    public long diffBetweenPorts() {
        return portRange.get(1) - portRange.get(0) + 1;
    }

    public long diffBetweenIP() {
        return ipRange.get(1) - ipRange.get(0) + 1;
    }
}
