package firewall;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Firewall {

    private Map<String, List<List<Long>>> ipToPort;
    private Map<String, List<List<Long>>> portToIp;

    public Firewall(String path) {

        ipToPort = new HashMap<>();
        portToIp = new HashMap<>();

        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader(path));
            String line;

            while ((line = br.readLine()) != null) {
                String[] rule = line.split(",");

                if (rule.length != 4) System.out.println("Invalid rule format!");

                Rule currRule = new Rule(rule[0], rule[1], rule[2], rule[3]);

                if (currRule.diffBetweenPorts() <= currRule.diffBetweenIP()) {
                    constructMap(portToIp, currRule.direction, currRule.protocol, currRule.portRange, currRule.ipRange);
                } else {
                    constructMap(ipToPort, currRule.direction, currRule.protocol, currRule.ipRange, currRule.portRange);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void constructMap(Map<String, List<List<Long>>> map, String direction, String protocol, List<Long> keyRange, List<Long> valueRange) {
        String key = direction + protocol;

        for (long i = keyRange.get(0); i <= keyRange.get(1); i++) {
            String newKey = key + i;

            if (!map.containsKey(newKey)) {
                map.put(newKey, new ArrayList<>());
            }

            updateValue(map, newKey, valueRange);
        }
    }

    private void updateValue(Map<String, List<List<Long>>> map, String key, List<Long> range) {
        List<List<Long>> list = map.get(key);

        int index =  binarySearch(list, range);
        list.add(index, range);
        mergeList(list, index);

        System.out.print("Key: " + key);
        System.out.println(" Value: " + list);
    }

    private int binarySearch(List<List<Long>> list,  List<Long> range) {
        if (list.size() == 0) {
            return 0;
        }

        int start = 0;
        int end = list.size() - 1;
        int mid;

        while (start + 1 < end) {
            mid = start + (end - start) / 2;

            if (list.get(mid).get(0) > range.get(0)) end = mid;
            else start = mid;
        }

        if (list.get(start).get(0) > range.get(0)) {
            return start;
        }

        if (list.get(end).get(0) > range.get(0)) {
            return end;
        }

        return list.size();
    }

    private void mergeList(List<List<Long>> list, int index) {
        while (index < list.size() - 1 && list.get(index).get(1) > list.get(index + 1).get(0)) {
            list.get(index).set(0, Math.min(list.get(index).get(0), list.get(index + 1).get(0)));
            list.get(index).set(1, Math.max(list.get(index).get(1), list.get(index + 1).get(1)));
            list.remove(index + 1);
        }

        while (index > 0 && list.get(index).get(0) < list.get(index - 1).get(1)) {
            list.get(index - 1).set(0, Math.min(list.get(index - 1).get(0), list.get(index).get(0)));
            list.get(index - 1).set(1, Math.max(list.get(index - 1).get(1), list.get(index).get(1)));
            list.remove(index);
            index--;
        }
    }

    public boolean acceptPacket(String direction, String protocol, int port, String ip) {

        long ipValue = Rule.getIp(ip);

        String keyPortToIp = direction + protocol + port;
        String keyIpToPort = direction + protocol + ipValue;

        if (portToIp.containsKey(keyPortToIp) && checkExist(portToIp.get(keyPortToIp), ipValue)) {
            return true;
        }

        if (ipToPort.containsKey(keyIpToPort) && checkExist(ipToPort.get(keyIpToPort), (long)port)) {
            return true;
        }

        return false;
    }

    private boolean checkExist(List<List<Long>> list, long target) {
        List<Long> temp = new ArrayList<>();
        temp.add(target);
        temp.add(target);

        int index = binarySearch(list, temp) - 1;
        if (index >= 0 && list.get(index).get(0) <= target && list.get(index).get(1) >= target) return true;

        return false;
    }

}
