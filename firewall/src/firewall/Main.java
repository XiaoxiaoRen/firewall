package firewall;

public class Main {

    public static void main(String[] args) {
        Firewall fw = new Firewall("/Users/xiaoxiaoren/Desktop/firewall/inputFile.csv");

        boolean res = fw.acceptPacket("inbound", "tcp", 80, "192.168.1.2");
        System.out.println(res == true);

        res = fw.acceptPacket("inbound", "udp", 53, "192.168.1.2");
        System.out.println(res == true);

        res = fw.acceptPacket("outbound", "tcp", 10234, "192.168.10.11");
        System.out.println(res == true);

        res = fw.acceptPacket("inbound", "tcp", 81, "192.168.1.2");
        System.out.println(res == false);

        res = fw.acceptPacket("inbound", "udp", 24, "52.12.48.92");
        System.out.println(res == false);

        res = fw.acceptPacket("inbound", "tcp", 90, "192.168.1.2");
        System.out.println(res == true);

        res = fw.acceptPacket("inbound", "tcp", 200, "192.168.1.2");
        System.out.println(res == true);

        res = fw.acceptPacket("inbound", "tcp", 506, "192.168.1.2");
        System.out.println(res == false);

        res = fw.acceptPacket("inbound", "tcp", 80, "192.168.3.7");
        System.out.println(res == true);

        res = fw.acceptPacket("inbound", "tcp", 80, "192.172.10.1");
        System.out.println(res == true);

        res = fw.acceptPacket("inbound", "tcp", 80, "192.168.3.0");
        System.out.println(res == false);

        res = fw.acceptPacket("inbound", "tcp", 82, "192.168.2.200");
        System.out.println(res == true);

        res = fw.acceptPacket("inbound", "tcp", 86, "192.168.2.200");
        System.out.println(res == false);

        res = fw.acceptPacket("inbound", "tcp", 82, "192.168.1.9");
        System.out.println(res == false);
    }
}
