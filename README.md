# Host-based Firewall Readme File

## Implementation

* Language: Java

* Data structure: HashMap + List

* Strategy: From the consideration of lookup speed, the fastest way is using a four level HashMap, where we can look up direction, protocol, port, and IP address separately at level 1, 2, 3, 4. This requires that we flatten all the ranges in rules which could cost unreasonably large memory(max 2 * 2 * 2^16 * 2^32 = 2^50), which might not be acceptable. So in order to save some space, I use three level HashMap instead of four, where the missing level I'm storing list of unique ranges instead. Since user is only allowed to specify range at port or IP address, we can choose either one as the missing level, which means two sets of HashMap. Given a rule, based on storing either port or IP as range will save us more space, we will insert this rule to corresponding HashMap.

## Testing

* Rules coverage:
  * Different direction
  * Different protocol
  * Only specify port as range
  * Only specify IP as range
  * Specify both port and IP as range
  * Specify both port and IP as value

* Packet coverage: For each rule, test both accepted and rejected case.

## Improvement

* I merged the three level HashMap as a single level one. I should separate them into three level which may improve the performance.
* Add more complete unit test.

## Team I'm interested in

Platform team > Policy team > Data team


